<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');



Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/users', [
    'uses' => 'UserController@index',
    'as' => 'users'
]);
Route::get('/companies', [
    'uses' => 'CompanyController@index',
    'as' => 'companies'
]);
Route::get('/users/create', [
    'uses' => 'UserController@create',
    'as' => 'users.create'
]);
Route::get('/users/delete/{id}', [
    'uses' => 'UserController@destroy',
    'as' => 'users.delete'
]);
Route::get('/users/edit/{id}', [
    'uses' => 'UserController@edit',
    'as' => 'users.edit'
]);
Route::post('/users/update/{id}', [
    'uses' => 'UserController@update',
    'as' => 'users.update'
]);
Route::post('/users/store', [
    'uses' => 'UserController@store',
    'as' => 'users.store'
]);

Route::get('/companies/create', [
    'uses' => 'CompanyController@create',
    'as' => 'companies.create'
]);
Route::get('/companies/delete/{id}', [
    'uses' => 'CompanyController@destroy',
    'as' => 'companies.delete'
]);
Route::get('/companies/edit/{id}', [
    'uses' => 'CompanyController@edit',
    'as' => 'companies.edit'
]);
Route::post('/companies/store', [
    'uses' => 'CompanyController@store',
    'as' => 'companies.store'
]);
Route::post('/companies/update/{id}', [
    'uses' => 'CompanyController@update',
    'as' => 'companies.update'
]);