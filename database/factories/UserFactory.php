<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'logo' => $faker->randomElement(['pic1.jpg', 'pic2.jpg', 'pic3.jpg']),
    ];
});


$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->email,
        'company_id' => \App\Company::all()->random()->id,
        'password' => bcrypt('password'), // secret
        'admin' => 0,
        'phone' => $faker->phoneNumber
    ];
});
