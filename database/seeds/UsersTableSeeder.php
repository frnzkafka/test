<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'admin@admin.com',
            'company_id' => 0,
            'password' => bcrypt('password'), // secret
            'admin' => 1,
            'phone' => '0697228215'
        ]);
    }
}
