@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Edit {{$user->first_name}}</div>
        <div class="card-body">
            <form method="post" action="{{route('users.update', ['id'=>$user->id])}}">
                {{csrf_field()}}
                {{var_dump($errors)}}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input value="{{$user->first_name}}" name="first_name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <input value="{{$user->last_name}}" name="last_name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input value="{{$user->email}}" name="email" type="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input value="{{$user->phone}}" name="phone" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="company">Company</label>
                    <select name="company_id">
                        @foreach($companies as $company)
                            <option value="{{$company->id}}"
                            @if($company->id == $user->company_id) selected @endif
                            >{{$company->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control">
                </div>
            </form>
        </div>
    </div>

@endsection