@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Create an employee</div>
        <div class="card-body">
            <form method="post" action="{{route('users.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input name="first_name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <input name="last_name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="email" type="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input name="phone" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="company">Company</label>
                    <select name="company_id">
                        @foreach($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control">
                </div>
            </form>
        </div>
    </div>

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">{{$error}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></div>
        @endforeach
    @endif

@endsection