@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Users
        <div class="card-link"><a href="{{route('users.create')}}">Create</a></div>
    </div>
    <div class="card-header">
        <table id="users" class="table display table-sm">
            <thead>
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Company</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->first_name}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->company->name}}</td>
                    <td><a class="btn btn-success" href="{{route('users.edit', ['id'=>$user->id])}}">Edit</a></td>
                    <td><a class="btn btn-danger" href="{{route('users.delete', ['id'=>$user->id])}}">Delete</a></td>

                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#users').DataTable({
            paging: true,
            lengthChange: false,
            searching:false,
        });
    } );
</script>

@endsection