@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Edit {{$company->name}}</div>
        <div class="card-body">
            <form method="post" action="{{route('companies.update', ['id'=>$company->id])}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input value="{{$company->name}}" name="name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input value="{{$company->email}}" name="email" type="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="logo">Logo</label>
                    <input name="logo" type="file" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control">
                </div>
            </form>
        </div>
    </div>

@endsection