@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Companies
            <div class="card-link"><a href="{{route('companies.create')}}">Create</a></div>
        </div>
        <div class="card-header">
            <table id="companies" class="table display table-sm">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Logo</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->name}}</td>
                        <td>{{$company->email}}</td>
                        <td><img width="50" height="50" src="{{asset($company->logo)}}"></td>
                        <td><a class="btn btn-success" href="{{route('companies.edit', ['id'=>$company->id])}}">Edit</a></td>
                        <td><a class="btn btn-danger" href="{{route('companies.delete', ['id'=>$company->id])}}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $('#companies').DataTable({
                paging: true,
                lengthChange: false,
                searching: false,
            });
        } );
    </script>

@endsection